# avg.prose.sh

A simple Markdown blog hosted at https://avg.prose.sh.

## Usage

A workflow automatically publishes to prose.sh directly using an SSH key known only to Forgejo. The contents of `blog/` get uploaded directly.

### Add a post

1. Create a Markdown file in `blog/` named with the post date and a title. (e.g. `blog/2023-12-31-hello-world.md`)
2. Commit the file to git and push to this repo.
3. Wait about a minute.
4. The new post should appear at https://avg.prose.sh.

### Rename a post

If you can simply edit the `title` frontmatter in the file contents, do that.

If you must change the filename, you must put an empty file in the place of the former filename in order for prose.sh to know to remove that post. (See [prose.sh docs](https://prose.sh/help#post-delete) for the reasons why.) You may remove that empty file in a future push.

## Contributing

Issues and pull requests are always welcome! You may sign in or create an account at [git.average.name](https://git.average.name) using your existing GitHub, GitLab, or Codeberg account via OAuth 2.0.

