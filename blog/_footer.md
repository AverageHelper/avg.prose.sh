<!-- See https://prose.sh/help#post-footer -->

_© 2023 Average Helper • Content is licensed [CC BY 4.0](https://git.average.name/AverageHelper/avg.prose.sh/src/branch/main/LICENSE)_

[support](https://average.name/support) | [mastodon](https://fosstodon.org/@avghelper) | [forgejo](https://git.average.name/AverageHelper) | [codeberg](https://codeberg.org/AverageHelper) | [source](https://git.average.name/AverageHelper/avg.prose.sh)

