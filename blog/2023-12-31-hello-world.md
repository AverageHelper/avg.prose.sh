---
title: "Hello, world!"
description: "My first post on prose.sh."
tags: [blog, meta]
date: 2023-12-31
---

UPDATE: The legacy blog has moved to https://old.blog.average.name. https://blog.average.name now points at Prose.sh.

# Hello, world!

I'm trying out hosting this blog at https://prose.sh. [Codeberg Pages](https://codeberg.page) currently serves my main blog, via a read-only Git mirror from [my own Forgejo instance](https://git.average.name/AverageHelper/blog). Should I move? Maybe. Not sure yet! I like the vibe of working in plaintext like this, without bothering with formatting and theme details. Zola's customizability is nice, but Prose's is so simple, it's hard to turn down!

Also, having someone else do the web host business is a load of overhead off my neck. The license file, too, can be [CC BY 4.0](https://git.average.name/AverageHelper/avg.prose.sh/src/branch/main/LICENSE) directly, instead of having to maintain separate licenses for code and content.

I'll start moving my old posts here from the old spot, and see if I like this. If I do, I'll update my https://blog.average.name CNAME to point here instead of Codeberg. I'm excited! I love playing with new sustainable tech toys :3

As always, this work will be open-source. You can find the source of this blog's content [on git.average.name](https://git.average.name/AverageHelper/avg.prose.sh).

