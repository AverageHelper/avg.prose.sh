---
title: "Leaving GitHub"
description: "GitHub no longer suits my purposes in writing and hosting open-source code. Since viable alternatives exist, I may as well leave."
tags: [cloud, foss, git, github, open-source]
date: 2023-12-16
---

# TL;DR

I'm leaving GitHub. Or at least, I'm working to slim my GitHub footprint. I'll still use GitHub when I need to, but my personal projects are moving to https://git.average.name.

# Background

Hey, long time no read!

I've been... busy. Life stuff hit hard, but a more detailed update is probably best suited to a separate blog post. Today's post is mainly an administrative update, and I think a more pressing matter.

# I'm moving from GitHub

I've begun moving my personal work to [Forgejo](https://forgejo.org). I host my own instance at https://git.average.name, and I've configured OAuth 2.0 such that most people should be able to contribute there using their existing GitHub, GitLab, or Codeberg credentials. (I tried to add Bitbucket to the mix, but Atlassian's acquisition has thrown Gitea's OAuth presets for a loop.)

I figured I should explain in a post why the move, rather than clogging my GitHub profile header with this silliness.

## GitHub is Not Git

GitHub has grown so popular itself that I've known people to conflate [Git](https://git-scm.com/) with GitHub. It's important not to confuse Git with your Git forge (GitHub, GitLab, Codeberg, Forgejo, etc.) This isn't really a reason to leave the GitHub platform, but something that I think bears repeating.

More to the point: GitHub has, for a long time, prided itself on being one of the best free Git forges out there, but its corporate approach leaves something to be desired.

Concerningly, despite GitHub's role in popularizing the concept of open-source software, GitHub's website is not open-source itself. Not a huge issue most of the time in practice, but on principle this behavior seems a bit... off. With most open-source codebases, any user with the know-how can create their own version, make the changes they want to see, and file a request to the code maintainers to pull those changes directly (in the form of a "Pull Request" or "Merge Request", depending on the forge). But to contribute to GitHub, the best we have is to get hired there, or file an issue at [GitHub Community](https://github.com/orgs/community/discussions) and hope someone high enough at GitHub takes notice.

Why go through that trouble when several competent open-source alternatives exist at [GitLab](https://about.gitlab.com) and [Codeberg](https://codeberg.org)?

## GitHub is Not a Git Company (Anymore)

On this day [just two years ago](https://web.archive.org/web/20211216000728/https://github.com/), GitHub advertised itself as literally everything I want in a Git forge:

- Public code hosting powered by Git
- Package registry
- An excellent code diffing view in Pull Requests
- Dead simple CI/CD integration
- and a few other nice-to-haves.

GitHub still does all of these things today. But it [now advertises itself](https://web.archive.org/web/20231216105815/https://github.com/) primarily as an "AI-powered developer platform".

My main issue with GitHub lately is that GitHub has shifted its focus: "Just as GitHub was founded on Git, today we are re-founded on Copilot." ([GitHub's CEO Thomas Dohmke, 8 Nov 2023](https://web.archive.org/web/20231215023455/https://github.blog/2023-11-08-universe-2023-copilot-transforms-github-into-the-ai-powered-developer-platform/)) GitHub may do a lot of Git-based things, but its self-proclaimed "re-found\[ing\]" on Microsoft's Copilot initiatives is _super_ not what free and open-source software is all about.

I don't need or want GitHub Codespaces. I'd much rather clone repositories and work on them locally at home or on a bus or a train without worrying about my web browser or internet connection mucking the works.

I don't need or want GitHub Copilot. I'd much rather write or rewrite code myself, really understand it, and make improvements as I go. That to me is the fun part of software engineering!

This "AI" craze is frustrating.

## A Secret Third Thing

It'd be nice to round out this post with "and here's GitHub's _third_ transgression, haha!" But, nothing comes to mind right now, and I can't be bothered to dig for something that doesn't really affect me much.

My last big "reason" for moving is that Forgejo is fun to host! Over the past week, this experience has done me lots of learning about reverse proxies and certificates with [Caddy](https://caddyserver.com/), hosting with [Linode](https://www.linode.com/), SSH, email servers, and some of the quirks of self-hosting my own Git forge. Now, I have my own cute miniature GitHub thingy, complete with its own automated CI/CD pipelines!

Why stick with GitHub when rolling my own is so much more fun? :grin:

# Conclusion

GitHub lately has developed a corporate stink to me. I don't fault anyone for sticking with it; GitHub seems to make compitent software most of the time. I plan to keep my account there indefinitely, if only so I can contribute to projects I enjoy.

I'm moving most of my code to [my own Forgejo instance](https://git.average.name). Anyone with an account on GitHub, GitLab, or Codeberg should, in theory, be able to make an account there to contribute to any of my code, if you want. Star, file issues, submit pull requests, whatever, same as GitHub! I plan to keep read-only Git mirrors on GitHub for repos that started there.

DM me [on Mastodon](https://fosstodon.org/@avghelper) if something's broken.

I just want to write useful software, with the help of anyone who wants to help. I don't need Microsoft's AI toys taking the fun out of it for me.

