---
title: "Getting Started"
description: "My very first blog post."
tags: [first, beginning, blog]
date: 2021-06-01
---

# First!

This it literally my first-ever blog post. It ain't much, but it's something.

I expect I'll make posts on here from time to time with regards to my open-source work, some software projects I'm paid for, and other subjects as I get time and inclination to write.

In short, don't expect much. I'm just some gal, ya know?

