---
nav:
  - support: https://average.name/support
  - mastodon: https://fosstodon.org/@avghelper
  - forgejo: https://git.average.name/AverageHelper
  - codeberg: https://codeberg.org/AverageHelper
  - source: https://git.average.name/AverageHelper/avg.prose.sh
---

<!-- See https://prose.sh/help#blog-readme -->

An average blog, playing around with Markdown and other text-based standards!

